function generate_pbs(pbsFolderBaseName, pbsFolderCoreName, videoFileName, frames, templateFileName, outFileName, coilSensitivityFile)
%GENERATE_PBS Summary of this function goes here
%   Detailed explanation goes here

pbsFolderName = sprintf('%s/%s',pbsFolderBaseName,pbsFolderCoreName);

mkdir(pbsFolderName);
mkdir([pbsFolderName,'/aux']);
mkdir([pbsFolderName,'/pbs']);
mkdir([pbsFolderName,'/pbs/jobs']);
mkdir([pbsFolderName,'/pbs/logs']);

copyfile('../functions/calckbkernel.m',[pbsFolderName,'/aux']);
copyfile('../functions/contour_tracker.m',[pbsFolderName,'/aux']);
copyfile('../functions/gridkb.m',[pbsFolderName,'/aux']);
copyfile('../functions/gridlut.m',[pbsFolderName,'/aux']);
copyfile('../functions/ift.m',[pbsFolderName,'/aux']);
copyfile('../functions/interpft2.m',[pbsFolderName,'/aux']);
copyfile('../functions/kb.m',[pbsFolderName,'/aux']);
copyfile('../functions/avi_to_struct.m',[pbsFolderName,'/aux']);
%copyfile('../functions/gridlut_mex.c',[pbsFolderName,'/aux']);
%copyfile('../functions/gridroutines.c',[pbsFolderName,'/aux']);
copyfile('../functions/gridlut_mex.mexa64',[pbsFolderName,'/aux']);

copyfile(templateFileName,[pbsFolderName, '/aux/template.mat'])
copyfile(videoFileName,[pbsFolderName,'/aux/videofile.avi']);

if ~isempty(coilSensitivityFile)
   copyfile(coilSensitivityFile,[pbsFolderName,'/aux/coilSensitivity.mat']); 
end;

fid=fopen([pbsFolderName,'/aux/main.m'],'w');
%fprintf(fid, 'mex /home/rcf-proj2/mv/toutios/%s/aux/gridlut_mex.c;\n',pbsFolderCoreName);
fprintf(fid, 'load template.mat; template=model;\n');
fprintf(fid, 'videostruct=avi_to_struct(''videofile.avi'');\n videodata=videostruct.frames(2:85,1:84,:);\n');
fprintf(fid, 'frames = %s\n',mat2str(frames));
fprintf(fid, 'numIterations = [10 90 300 300];\n');
if ~isempty(coilSensitivityFile)
    fprintf(fid, 'coilIntensityCorrectionOn = 1;\n');
else
    fprintf(fid, 'coilIntensityCorrectionOn = 0;\n');
end
fprintf(fid, 'newtonMethodOn = 0;\n');
fprintf(fid, 'contourCleanUpOn = 1;\n');
fprintf(fid, 'plotOn = 0;\n');
fprintf(fid, 'notifyOn = 0;\n');
fprintf(fid, 'parallelOn = 1;\n');
fprintf(fid, 'workers = 16;\n');
fprintf(fid, 'coilSensitivityMatFileName = ''coilSensitivity.mat'';\n');

fprintf(fid, 'trackdata = contour_tracker(videodata, template, numIterations,...\n');
fprintf(fid, '    coilIntensityCorrectionOn, coilSensitivityMatFileName, ...\n');
fprintf(fid, '    newtonMethodOn, contourCleanUpOn, plotOn, notifyOn, frames, ...\n');
fprintf(fid, '    parallelOn, workers)\n');

fprintf(fid, 'save(''%s'',''trackdata'')\n',outFileName);

fclose(fid);

fid=fopen(sprintf('%s/pbs/jobs/pbsjob_main.sh',pbsFolderName),'w');

fprintf(fid, 'cp -rf /home/rcf-proj2/mv/toutios/%s/ /tmp/\n',pbsFolderCoreName);
fprintf(fid, 'source /usr/usc/matlab/default/setup.sh\n');
fprintf(fid, 'matlab -r \"addpath(\''/tmp/%s/\'');addpath(\''/tmp/%s/aux\'');main;exit;\"\n',pbsFolderCoreName,pbsFolderCoreName);
fprintf(fid, 'rsync -av /tmp/%s/aux/ /home/rcf-proj2/mv/toutios/%s/aux/\n',pbsFolderCoreName,pbsFolderCoreName);

fclose(fid);

fid=fopen(sprintf('%s/pbs/pbs_main',pbsFolderName),'w');

fprintf(fid, '#PBS -l walltime=23:59:59\n');
fprintf(fid, '#PBS -l mem=4gb\n');
fprintf(fid, '#PBS -l nodes=1:ppn=16\n');
fprintf(fid, '#PBS -o /home/rcf-proj2/mv/toutios/%s/pbs/logs/output_main.txt\n',pbsFolderCoreName);
fprintf(fid, '#PBS -e /home/rcf-proj2/mv/toutios/%s/pbs/logs/error_main.txt\n\n',pbsFolderCoreName);
fprintf(fid, 'pbsdsh -n 0 bash /home/rcf-proj2/mv/toutios/%s/pbs/jobs/pbsjob_main.sh &\n\n',pbsFolderCoreName);
fprintf(fid, 'wait\n');

fclose(fid);

end

