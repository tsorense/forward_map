function make_track_video(avifile,contourfile)

videostruct = avi_to_struct(avifile);
videodata = videostruct.frames;

load(contourfile);

close all;

set(gca,'NextPlot','replaceChildren');

nmovie = size(videodata,3);
width = size(videodata,1);

writerObj = VideoWriter(strrep(contourfile,'.mat','.avi'));
writerObj.FrameRate = videostruct.framerate;
open(writerObj);

%readObj = VideoReader(videofile);

dataindex=1;

close all; figure;

for movieframe=1:nmovie
    
    img=videodata(:,:,movieframe);
    
    %imagesc([-(width-1)/2 (width-1)/2], [-(width-1)/2 (width-1)/2],img); 
    imshow(mat2gray(img), 'XData',[-(width-1)/2 (width-1)/2],'YData',[-(width-1)/2 (width-1)/2],'Border','tight','InitialMagnification',1000); hold on;
    %imshow(f,imshow(f,'XData',[-41.5 41.5],'YData',[-41.5 41.5]); %hold on;,[-41.5 41.5],'YData',[-41.5 41.5]); %hold on;
    %colormap(gray); axis equal; axis off; hold on;
    
    if dataindex <= size(trackdata,2)
        dataframe=trackdata{dataindex}.frameNo;
    else
        dataframe=0;
    end;
    
    if ~isempty(dataframe)
        
        if movieframe==dataframe
            
            segment=trackdata{dataindex}.contours.segment;
            
            for s=1:(size(segment,2)-1)
                sectionsId = segment{s}.i;
                v          = segment{s}.v;
                colors = ['r' 'g' 'b' 'y' 'c' 'm' 'k'];
                for sId=1:max(sectionsId)
                    plot( v(sectionsId==sId,1),-v(sectionsId==sId,2),colors(sId),'LineWidth',4); hold on;
                end;
            end;
            dataindex=dataindex+1;
            
        end;
        
    end;
    
    hold off;

    drawnow;
    writeVideo(writerObj,getframe);
    
end;

close(writerObj);
