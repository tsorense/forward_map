function generate_pbs_batch(pbsBaseFolderName, scriptName, segmentFile, firstLine, lastLine, frameRate, templateFileName, coilSensitivityFile, videoFilePath, clusterDir, wallhours)

lineCount = 1;

bashFileName = sprintf('%s/%s.sh', pbsBaseFolderName, scriptName);
 
fid = fopen(segmentFile);
fidout = fopen(bashFileName,'w');
fprintf(fidout, '#!/bin/bash\n');

while lineCount<firstLine
    
    fgetl(fid);
    lineCount = lineCount + 1;
    
end;

while lineCount<lastLine+1
    
    str = fgetl(fid);
    celldata = textscan(str,'%s','Delimiter',',');
    strdata = celldata{1};
    videoFileName = [videoFilePath char(strdata(1))];
    pbsCoreFolderName = char(strdata(2));
    outFileName = [pbsCoreFolderName,'_track'];
      
    frames=[];
    
    for segment = 1:(length(strdata)-2)/2
    
    segmentstartframe = floor(frameRate/1000*str2double(strdata(2*segment+1)));
    segmentendframe = ceil(frameRate/1000*str2double(strdata(2*segment + 2)));
    
    frames = [frames, segmentstartframe:segmentendframe];
    
    end;
    
    generate_pbs(pbsBaseFolderName, pbsCoreFolderName, videoFileName, frames, templateFileName, outFileName, coilSensitivityFile, clusterDir, wallhours);
    
    fprintf(fidout, 'qsub %s/pbs/pbs_main\n',pbsCoreFolderName);
    
    lineCount = lineCount + 1;
    
end;

fclose(fid);
fclose(fidout);

