function videostruct = avi_to_struct(avifile)

% function videostruct = avi_to_struct(avifile)
%
% Converts an avi file to a video structure
%
% Asterios Toutios, Sep 9 2015

% Determine number of frames

vidObj = VideoReader(avifile);

nmovie = get(vidObj, 'NumberOfFrames');

% Determine dimensions of frame an Framerate

xdim = get(vidObj, 'Height');
ydim = get(vidObj, 'Width');

framerate = get(vidObj, 'FrameRate');

% Reset

vidObj = VideoReader(avifile);

% Initialize

videostruct.frames = zeros(xdim,ydim,nmovie);
videostruct.framerate = framerate;

% Read
f=read(vidObj);
for i=1:nmovie
    
    frame = f(:,:,:,i);
    
    if ismatrix(frame)
        videostruct.frames(:,:,i) = frame;
    else
        videostruct.frames(:,:,i) = rgb2gray(frame);
    end;
    
end;


end

