function plotRelativeContributions(datPath,graphicsPath,subj2)

rng(19)
load([datPath 'ratios.mat'])
load([datPath '/clusters.mat'])
subj = fields(clusters);
[subj2,ii] = sort(subj2);
subj = subj(ii);
fntsz = 20;
txtLocs = [1 2 3 3.8 4.9];

green = [0 0.7 0.18];
red = [0.86 0.13 0];
clmin = 0;
clmax = 1;
titl = {'relative contribution of lips',...
    'relative contribution of tongue',...
    'relative contribution of tongue',...
    'relative contribution of tongue',...
    'relative contribution of tongue'};
task = {'bilabial closure','alveolar closure','palatal approximation','velar closure','pharyngeal approximation'};
task2 = {'bilabial','alveolar','palatal','velar','pharyngeal'};
[Nz,~] = size(clusters.(subj{1}).jac{1});   % No. task variables z
                                               % No. model articulators phi

% for i=1:Nz-1
%     
%     zzj = squeeze(tot_jaw(ii,i,:));
%     zzlt = squeeze(tot_lt(ii,i,:));
%     zz = zzlt./(zzlt+zzj);
%     
%     disp([task{i} ': ' num2str(sum(mean(zz,2)>0.5))])
%     disp(num2str(mean(zz(:)>0.5)))
%     
%     figure('Color','w')
%     imagesc(zzlt./(zzlt+zzj))
%     colormap(gray)
%     if i==2
%         c=colorbar;
%     end
%     %title(task{i},'FontSize',fntsz)
%     set(gca,'clim',[clmin clmax],'YTick',1:length(subj2),'YTickLabel',cell(length(subj2),1),'XTick',1:4,'XTickLabel',30:20:90,'FontSize',14)
%     if i==1 || i==3
%         set(gca,'YTickLabel',subj2)
%     end
%     
%     axis tight
%     
%     % create a PDF file
%     fnam=[graphicsPath 'contributions_' num2str(i) '.pdf'];
%     export_fig(fnam);
%     
% end

figID = figure('Color','w');
x_am1 = ones(Nz-1,1);
y_am1 = ones(Nz-1,1);
x_vl1 = ones(Nz-1,1);
y_vl1 = ones(Nz-1,1);
for i=1:Nz-1
    zzj = squeeze(tot_jaw(ii,i,:));
    zzlt = squeeze(tot_lt(ii,i,:));
    zz = zzlt./(zzlt+zzj);
    
    figure(figID)
    xx=i+0.05.*randn(size(zz,1),1);
    yy=mean(zz,2);
    scatter(xx,yy,60,'o','k'), hold on
    
    x_am1(i) = xx(strcmp(subj,'am1'));
    y_am1(i) = yy(strcmp(subj,'am1'));
    x_vl1(i) = xx(strcmp(subj,'vl1'));
    y_vl1(i) = yy(strcmp(subj,'vl1'));
    
    if i==1
        selected_subj = {'am1','vl1'};
        indx = strcmp(subj,selected_subj{1}) | strcmp(subj,selected_subj{2});
%         scatter(xx(indx), yy(indx), 100, [red; green],'filled')
    elseif i==2
        selected_subj = {'am1','vl1'};
        indx = strcmp(subj,selected_subj{1}) | strcmp(subj,selected_subj{2});
%         scatter(xx(indx), yy(indx), 100, [red; green], 'filled')
    end
    plot(i+[-0.2 0.2],repmat(median(mean(zz,2)),1,2),'k','LineWidth',2)
    text(txtLocs(i),-0.05,task2{i},'FontSize',fntsz,'HorizontalAlignment','center');
end

% plot(x_am1,y_am1,'Color',green,'LineWidth',2)
% scatter(x_am1,y_am1,100,green,'filled')
% text(5.35,y_am1(end),'M3','FontSize',fntsz,'HorizontalAlignment','center')
% plot(x_vl1,y_vl1,'Color',red,'LineWidth',2)
% scatter(x_vl1,y_vl1,100,red,'filled')
% text(5.35,y_vl1(end),'F9','FontSize',fntsz,'HorizontalAlignment','center')

set(gca,'YTick',0:1,'YTickLabel',{'all jaw','no jaw'},'XTick',[],'FontSize',fntsz)

xlim([0.5 5.5])
ylim([0 1])
set(gca,'LooseInset',get(gca,'TightInset')+[0 0.1 0 0])
savefig([graphicsPath 'contributions_aggregate'],'png')

end