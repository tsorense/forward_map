function [jawIndx,tngIndx,lipIndx,velIndx,larIndx,headIndx] = getIndxs(names)

jawIndx = cellfun(@(x)~isempty(strfind(x,'jaw')),names);
tngIndx = cellfun(@(x)~isempty(strfind(x,'tongue')),names);
lipIndx = cellfun(@(x)~isempty(strfind(x,'lip')),names);
velIndx = cellfun(@(x)~isempty(strfind(x,'vel')),names);
larIndx = cellfun(@(x)~isempty(strfind(x,'larynx')),names);
headIndx = cellfun(@(x)~isempty(strfind(x,'head')),names);

end