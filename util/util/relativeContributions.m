function relativeContributions(datPath)

load([datPath '/clusters.mat'])
load([datPath '/gfa.mat'])
load([datPath '/tv.mat'])
subj = fields(clusters);
nDir = length(subj);

zPhiRel = logical([0 0 0 0 0 1 1 0;...
    0 1 1 1 1 0 0 0; ...
    0 1 1 1 1 0 0 0; ...
    0 1 1 1 1 0 0 1; ...
    0 1 1 1 1 0 0 0; ...
    0 0 0 0 0 0 0 1]);

[Nz,Nphi] = size(clusters.(subj{1}).jac{1});   % No. task variables z
                                               % No. model articulators phi
dot = Nphi+1:2*Nphi;        % indices

% parameters of the simulation
W = eye(Nphi);
time = 0.3;              % time in sec
n_frames = 20;           % No. frames in which to linearize ODE
h1 = time./n_frames;
h2 = h1./n_frames;         % time step
q = 0.3:0.2:0.9;
n_samples = length(q);
n_best = 3; %5;
n_timestamps = time./h2;

% results containers
tot_lt = NaN(nDir,Nz,n_samples);
tot_jaw = NaN(nDir,Nz,n_samples);
tot_z = NaN(nDir,Nz,n_samples);
tot = NaN(nDir,Nz,n_samples);
init = NaN(nDir,Nz,n_samples);

for h=1:nDir
    disp(['subject ' num2str(h) '/' num2str(nDir)])
    
    names = gfa.(subj{h}).names;
    spatRes = tv.(subj{h}).spatRes;
    
    % trim clusters
    nClusters = length(clusters.(subj{h}).fwd);
    indx = false(nClusters,1);
    for i=1:nClusters
        indx(i) = any(abs(sum(clusters.(subj{h}).jac{i},2)) < 1e-1);
    end
    clusters.(subj{h}).centers(indx,:) = [];
    clusters.(subj{h}).fwd(indx) = [];
    clusters.(subj{h}).jac(indx) = [];
    clusters.(subj{h}).jacDot(indx) = [];
    
    centers = clusters.(subj{h}).centers;
    fwd = clusters.(subj{h}).fwd;
    jac = clusters.(subj{h}).jac;
    jacDot = clusters.(subj{h}).jacDot;
    
    for i=1:Nz
        disp(['-constriction ' num2str(i) '/' num2str(Nz)])
        
        init(h,i,:) = quantile(tv.(subj{h}).tv{i}.cd,q);
        omega = zeros(Nz,1); % natural frequencies of task variables
        omega(i) = 25;
        z0 = [0 0 4 0 7 0]'./spatRes;     % constriction degree targets
        
        for j=1:n_samples
            zInit = zeros(Nz,1);
            zInit(i) = init(h,i,j);
            indx = getNearestCluster([],centers,names,'z',fwd,zInit,n_best);
            
            % interim results containers
            lt_contribution=zeros(n_best,n_timestamps);
            jaw_contribution=zeros(n_best,n_timestamps);
            
            for k=1:n_best
                phiInit = [centers(indx(k),:) zeros(1,Nphi)]';

                % solve the task dynamics
                [t,phi,z,clusterID] = task_dynamics(omega,z0,h2,h1,n_frames,phiInit,W,centers,fwd,jac,jacDot,Nz,Nphi,names);

                phiDot_lt = zeros(Nphi,n_timestamps);
                phiDot_lt(zPhiRel(i,:),:) = phi(dot(zPhiRel(i,:)),:);
                phiDot_jaw = zeros(Nphi,n_timestamps);
                phiDot_jaw(1,:) = phi(dot(1),:);

                for ell=1:n_timestamps
                    J = jac{clusterID(ell)};
                    lt_contribution(k,ell) = J(i,:)*phiDot_lt(:,ell);
                    jaw_contribution(k,ell) = J(i,:)*phiDot_jaw(:,ell);
                end
            end
            
            tot_lt(h,i,j) = median(trapz(t,lt_contribution,2));
            tot_jaw(h,i,j) = median(trapz(t,jaw_contribution,2));
            tot(h,i,j) = tot_lt(h,i,j) + tot_jaw(h,i,j);
        end
    end
end

save([datPath 'ratios.mat'],'tot_lt','tot_jaw','tot','init')

end