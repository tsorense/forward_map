function rm_nonlinear_clusters(pathOut)
%RM_NONLINEAR_CLUSTERS - remove clusters which are not approximately linear
%from file 'clusters.mat' and save the resulting file as
%'linear_clusters.mat'.
% 
% input: 
%  PATHOUT - path to save output
% 
% Last Updated: Oct. 13, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

load(fullfile(pathOut, 'clusters.mat'))
subj = fields(clusters);
nsubj = length(subj);
for i=1:nsubj
    keepIndx = logical(clusters.(subj{i}).linear);
    clusters.(subj{i}).centers = clusters.(subj{i}).centers(keepIndx,:);
    clusters.(subj{i}).fwd = clusters.(subj{i}).fwd(keepIndx);
    clusters.(subj{i}).jac = clusters.(subj{i}).jac(keepIndx);
    clusters.(subj{i}).jacDot = clusters.(subj{i}).jacDot(keepIndx);
end

save(fullfile(pathOut,'linear_clusters.mat'),'clusters')


end

