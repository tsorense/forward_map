function [z,w,centers,b] = getPoints

Npt = 500;     % no. points
Ncl = 5;        % no. clusters
ppc = Npt/Ncl;  % points per cluster (should be integer)

% clusters on weight plane
centers = -100 + (100+100).*rand(2,Ncl);

% z = b*w, matrix b differs by cluster
b = -1 + (1+1).*rand(Ncl,2);

w = zeros(2,Npt); % weights
z = zeros(1,Npt); % task variable

for i=1:Ncl
    ii = (i-1)*ppc+1:(i-1)*ppc+ppc;
    w(:,ii) = repmat(centers(:,i),1,ppc)+randn(2,ppc);
    z(ii) = b(i,:)*w(:,ii);
end
w = w';
z = z';




end