function [mu,sigma,n] = count_samples(datPath)

load([datPath 'contourdata.mat'])
subj = fields(contourdata);

for h=1:length(subj)
    n(h) = size(contourdata.(subj{h}).X,1);
end

mu = mean(n);
sigma = std(n);


end