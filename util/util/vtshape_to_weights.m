function weights = vtshape_to_weights(xy_data, mean_vtshape, U);

norm_data = xy_data-mean_vtshape;

weights = norm_data*U;