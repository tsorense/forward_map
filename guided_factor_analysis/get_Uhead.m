function U_head = get_Uhead(contourdata,verbose)

SectionsID=contourdata.SectionsID;
D=[contourdata.X,contourdata.Y];
meandata=ones(size(D,1),1)*mean(D);
Dnorm=D-meandata;
vtsection=11; %palate + nasal
SecID2=[SectionsID,SectionsID];
Dnorm(:,~ismember(SecID2,vtsection))=0;
[U,V,varpercent,m]=pca(Dnorm);

if verbose==true
    close all;
    figure(1);
    for i=1:3
        figure(i);
        DD = Dnorm*U(:,i)*pinv(U(:,i));
        plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
        plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(mean(D),SectionsID(1,:),'k');
        text(-15,-25,sprintf('%3.2f',varpercent(i)))
        axis equal; axis off;
    end;
end

U_head=U(:,1:3);

end