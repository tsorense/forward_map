function U_tng = get_Utng(contourdata,U_jaw,nTng,verbose)

D=[contourdata.X,contourdata.Y];
Nobs = size(D,1);
meandata=ones(Nobs,1)*mean(D);
Dnorm=D*(eye(size(D,2))-U_jaw*pinv(U_jaw))-meandata;
vtsection=2; % tongue
SectionsID=contourdata.SectionsID;
SecID2=[SectionsID,SectionsID];
Dnorm(:,~ismember(SecID2,vtsection))=0;
[U,~,~,~,varpercent]=pca(Dnorm);
U_tngraw=U;
U = zeros(size(U_tngraw,1),nTng);

%%% GFA
R = Dnorm'*Dnorm/Nobs; % covariance matrix

for i=1:nTng
    t1=U_tngraw(:,i);
    v=t1'*R*t1;
    h1=t1/sqrt(v);
    f1=(h1'*R)';
    R=R-f1*f1';
    U(:,i)=f1;
end

% GFA Overall
if verbose==true
    close all
    for i=1:nTng
        figure
        DD = Dnorm*U(:,i)*pinv(U(:,i));
        plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
        plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(mean(D),SectionsID(1,:),'k');
        text(-15,-25,sprintf('%3.2f',varpercent(i)))
        axis equal; axis off;
    end
end

U_tng=U(:,1:nTng);

end