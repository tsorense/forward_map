function U_lar = get_Ular(contourdata,U_jaw,verbose)

SectionsID=contourdata.SectionsID;
D=[contourdata.X,contourdata.Y];
D=D-D*U_jaw*pinv(U_jaw);
meandata=ones(size(D,1),1)*mean(D);
Dnorm=D-meandata;
SecID2=[SectionsID,SectionsID];
indarytenoid=[82:87, 266:271]; % check
Dnorm(:,setdiff(1:length(SecID2),indarytenoid))=0;
[U,V,varpercent,m]=pca(Dnorm);

if verbose==true
    close all;
    for i=1:2
        figure(i);
        DD = Dnorm*U(:,i)*pinv(U(:,i));
        plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
        plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(mean(D),SectionsID(1,:),'k');
        text(-15,-25,sprintf('%3.2f',varpercent(i)))
        axis equal; axis off;
    end;
end

U_lar=U;

end