function plotGFA(datPath,graphicsPath)

% Load data of contours, weights, and model.
load([datPath 'gfa.mat'])
subj = fields(gfa);
nDir = length(subj);

gry = [0.5 0.5 0.5];

FactorSegments = {[1 2 3 4 5 15], [1 2], [1 2], [1 2], [1 2], [4 15], ...
    [4 15], [12], [11], [11]};
% 1 epiglottis
% 2 tongue
% 3 jaw bone
% 4 lower lip
% 5 chin
% 6 trachea front
% 7 pharyngeal wall
% 8 FOV top border
% 9 FOV back border
% 10 FOV bottom border
% 11 hard palate
% 12 velum
% 13 nasal cavity
% 14 nose
% 15 upper lip

for h=1:nDir
    
    U = gfa.(subj{h}).U_gfa;
    nf = size(U,2);
    SectionsID = gfa.(subj{h}).SectionsID;
    mean_vtshape = gfa.(subj{h}).mean_vtshape;
    names = gfa.(subj{h}).names;
    
    for i=1:length(names);  % component under examination
        fs = FactorSegments{i};
        plotSegs = zeros(1,length(SectionsID));
        for j=1:length(fs)
            plotSegs = plotSegs + double(SectionsID==fs(j));
        end
        plotSegs = logical(plotSegs);
        
        figID = figure('Color','w');
        parameters=zeros(1,nf);
        sigma = std(gfa.(subj{h}).weights(:,i));
        parameters(i)=1*sigma;
        plot_from_xy(mean_vtshape,SectionsID(1,:),gry); hold on;
        xy_data = weights_to_vtshape(parameters,mean_vtshape, U);
        plot_from_xy(xy_data, ...
            SectionsID(1,:) .* plotSegs,'k'); hold on;
        parameters(i)=-1*sigma;
        xy_data = weights_to_vtshape(parameters, mean_vtshape, U);
        plot_from_xy(xy_data, ...
            SectionsID(1,:) .* plotSegs,'k'); hold off;
        
        axis tight
        v=axis;
        text(v(1),v(3)+0.1.*(v(4)-v(3)),names{i},'FontSize',28);
        axis off;
        
        % create a PDF file
        fnam=[graphicsPath subj{h} '_factor' num2str(i)];
        export_fig(fnam);
        close(figID)
    end
end

end