function get_Ugfa(pathOut,nf,verbose)
% GET_UGFA - performs guided factor analysis as described in the following
% reference:
% 
% Asterios Toutios, Shrikanth S. Narayanan, "Factor analysis of vocal-tract
% outlines derived from real-time magnetic resonance imaging data", 
% International Congress of Phonetic Sciences (ICPhS 2015), Glasgow, UK, 
% 2015. 
% 
% Last Updated: Oct. 13, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

warning('off','stats:pca:ColRankDefX')

allcontourdata = load(fullfile(pathOut, 'contourdata.mat'));
allcontourdata = allcontourdata.contourdata;

folders = fields(allcontourdata);
nDir = length(folders);

for h = 1:nDir
    disp(['Extracting factors for subject ' folders{h}]);
    
    contourdata = allcontourdata.(folders{h});
    U = [];
    
    U_jaw = get_Ujaw(contourdata,nf.nJaw,folders{h},verbose);
    U=cat(2,U,U_jaw(:,1:nf.nJaw));

    U_tng = get_Utng(contourdata,U_jaw,nf.nTng,verbose);
    U=cat(2,U,U_tng(:,1:nf.nTng));

    U_lip = get_Ulip(contourdata,U_jaw,nf.nLip,verbose);
    U=cat(2,U,U_lip(:,1:nf.nLip));

    U_vel = get_Uvel(contourdata,verbose);
    U=cat(2,U,U_vel(:,1:nf.nVel));

    U_head = get_Uhead(contourdata,verbose);
    U=cat(2,U,U_head(:,1:nf.nHead));
    
    D=[contourdata.X,contourdata.Y];
    meandata=ones(size(D,1),1)*mean(D);
    Dnorm=D-meandata;
    
    contourdata.weights=(U'*Dnorm')';
    n = size(D,1);
    contourdata.parameters=(contourdata.weights-repmat(mean(contourdata.weights,1),n,1))./repmat(std(contourdata.weights,1),n,1);
    contourdata.U_gfa = U;
    contourdata.mean_vtshape = mean(D);
    contourdata.subj{h} = folders{h};
    gfa.(folders{h})=contourdata;
end

save(fullfile(pathOut, 'gfa.mat'), 'gfa')