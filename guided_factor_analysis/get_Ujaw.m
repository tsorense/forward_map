function U_jaw = get_Ujaw(contourdata,nJaw,name,verbose)

D=[contourdata.X,contourdata.Y];
n = size(D,1);
meandata=ones(n,1)*mean(D);
Dnorm=D-meandata;
vtsection=5; %jaw (but not incisor)
SectionsID=contourdata.SectionsID;
SecID2=[SectionsID,SectionsID];
Dnorm(:,~ismember(SecID2,vtsection))=0;
[U,~,~,~,varpercent]=pca(Dnorm);

if verbose==true
    close all
    for i=1:nJaw
        figure;
        DD = Dnorm*U(:,i)*pinv(U(:,i));
        plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
        plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(mean(D),SectionsID(1,:),'k');
        axis equal; axis off;
        text(-15,-25,sprintf('%3.0f',varpercent(i)))
        print(sprintf('jaw%d_%s',i,name),'-dpng')
    end
end

U_jawraw=U;
U = zeros(size(U_jawraw,1),nJaw);

%%% GFA
vtsection=1:6;
SectionsID=contourdata.SectionsID;
D=[contourdata.X,contourdata.Y];
meandata=ones(size(D,1),1)*mean(D);
Dnorm=D-meandata;
SecID2=[SectionsID,SectionsID];
Dnorm(:,~ismember(SecID2,vtsection))=0;
Nobs = size(D,1);

%%% GFA
R = Dnorm'*Dnorm/Nobs; % covariance matrix

for i=1:nJaw
    t1=U_jawraw(:,i);
    v=t1'*R*t1;
    h1=t1/sqrt(v);
    f1=(h1'*R)';
    R=R-f1*f1';
    U(:,i)=f1;
end

if verbose==true
    % plotting
    figure
    DD = Dnorm*U*pinv(U);
    plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
    plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
    plot_from_xy(mean(D),SectionsID(1,:),'k');
    axis equal; axis off;
    print(sprintf('jawtngllip_%s',name),'-dpng')
end

U_jaw=U(:,1:nJaw);

end