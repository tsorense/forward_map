%addpath ../functions
%load ./contourdata_original.mat

%contourdata=contourdata_pca;

SectionsID=contourdata.SectionsID;

D=[contourdata.X,contourdata.Y];

meandata=ones(size(D,1),1)*mean(D);

Dnorm=D-meandata;

vtsection=[1:15]; %all

% 01 Epiglottis
% 02 Tongue
% 03 Incisor
% 04 Lower Lip
% 05 Jaw
% 06 Trachea
% 07 Pharynx
% 08 Upper Bound
% 09 Left Bound
% 10 Low Bound
% 11 Palate
% 12 Velum
% 13 Nasal Cavity
% 14 Nose
% 15 Upper Lip

SecID2=[SectionsID,SectionsID];

Dnorm(:,~ismember(SecID2,vtsection))=0;

[U,V,varpercent,m]=pca(Dnorm,368);

close all;

figure(1);

for i=1:3
    
    figure(i);
    
    DD = Dnorm*U(:,i)*pinv(U(:,i));

plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),0.4); hold on;
plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),0.7); hold on;

plot_from_xy(mean(D),SectionsID(1,:),0);

    
    axis equal; axis off;
    
    text(-15,-25,sprintf('%3.2f',varpercent(i)))
        
    %title(['Component ',num2str(i),', Total Variance Explained: ',num2str(varpercent(i))]);
    
end;


U_pca=U;


save ./U_pca.mat U_pca
