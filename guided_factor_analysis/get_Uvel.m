function U_vel = get_Uvel(contourdata,verbose)

SectionsID=contourdata.SectionsID;
D=[contourdata.X,contourdata.Y];
meandata=ones(size(D,1),1)*mean(D);
Dnorm=D-meandata;
vtsection=[12]; %Velum
SecID2=[SectionsID,SectionsID];
Dnorm(:,~ismember(SecID2,vtsection))=0;
[U,V,varpercent,m]=pca(Dnorm);

if verbose==true
    close all;
    for i=1:2
        figure(i);
        DD = Dnorm*U(:,i)*pinv(U(:,i));
        plot_from_xy(mean(D)+2*std(DD),SectionsID(1,:),'r'); hold on;
        plot_from_xy(mean(D)-2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(mean(D),SectionsID(1,:),'k');
        text(-15,-25,sprintf('%3.2f',varpercent(i)))
        axis equal; axis off;
    end;
end

U_vel=U;

end