% WRAPPER - wraps function MAIN, which estimates the forward kinematic map
% and runs simulations
% 
% Constriction locations can be set manually by running TVLOCS.m before 
% WRAPPER using the following command in the MATLAB terminal:
%   >> tvlocs(config.mat_path)
% Note that the relevant config file must be loaded. In order for the
% manually set constriction locations to be used, set ncl in the config
% file to an empty array. Otherwise, constriction locations are discovered
% automatically. 
% 
% Last Updated: Nov. 18, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

addpath(genpath(pwd))                % add subdirectories to MATLAB path
config_file = input('Enter config file name: ','s');
load(config_file)

% path variables
pathIn = config.tracks_path;         % path to track files
pathOut = config.mat_path;           % path to save output
graphicsPath = config.graphics_path; % path to save graphics

% MRI parameters
fov = config.fov;                    % field of view (mm^2)
nPixel = config.npixel;              % nPixel^2 is total pixels in MRI frame
spatRes = fov/nPixel;                % spatial resolution (mm)
TR = config.TR;                      % TR (s)
frameRate = 1/(config.TRperframe*TR);% frame rate

% other
crit = spatRes*config.tol;          % error tolerance for linear approx.
ncl = config.ncl;                   % number of constriction locations at 
                                    %  (1) palate, (2) hypopharynx
q = config.q;                       % quantile below which constrictions 
                                    % are considered for constriction 
                                    % locations.
nf = config.nf;                     % number of factors in factor model
verbose = false;                    % controls output to MATLAB terminal

% estimate forward kinematic map, run simulations
main(pathIn,pathOut,graphicsPath,spatRes,...
    crit,frameRate,ncl,q,nf,verbose)
