function [t,phi,z,clusterID] = task_dynamics(omega,z0,h1,h2,n_frames,phiInit,W,centers,fwd,jac,jacDot,Nz,Nphi,nf)

plain = 1:Nphi;             % indices
dot = Nphi+1:2*Nphi;        % indices

% (i,j)-entry is 1 if weight j is used to change task variable i
zPhiRel = [1 1 1 0 0 0 0 1 1 0; ...
    0 0 0 0 0 0 0 0 0 1; ...
    1 1 1 1 1 1 1 0 0 0; ...
    1 1 1 1 1 1 1 0 0 0; ...
    1 1 1 1 1 1 1 0 0 0];

% parameters of the orthogonal projection operator
G_P = diag(zPhiRel'*double(omega~=0));

% parameters of the neutral gesture
% (see Saltzman & Munhall, 1989, Appendix A)
omega_N = 10;
B_N = 2*omega_N*eye(Nphi);
K_N = omega_N^2.*eye(Nphi);
G_N = eye(Nphi)-G_P; % equiv.: diag(~(zPhiRel'*double(omega~=0)));

% parameters of flow on task mass Z
K = diag(omega.^2);
B = diag(2.*omega);

% parameters of the simulation
% parameters of articulators PHI
n_timestamps = h2.*n_frames./h1;
clusterID = zeros(1,n_timestamps);
z = NaN(Nz,n_timestamps);
t = NaN(1,n_timestamps);
t(1) = 0;
tspan = h1:h1:h2;
phi = NaN(2.*Nphi,n_timestamps);
phi(:,1) = phiInit;

% locally linearize the ODE over N_FRAMES frames
for i=1:n_frames
    indx1 = getNearestCluster(phiInit,centers,nf);
    indx2 = (i-1).*n_frames+1:i.*n_frames;
    
    F = fwd{indx1};
    J = jac{indx1};
    J_t = jacDot{indx1};
    Jstar = jacStar(J,W,diag(omega~=0),Nz); % weighted pseudoinverse
    
    % flow on Z
    f=@(t,phi)[phi(dot);Jstar*(-B*J*phi(dot)-K*(F*[1;phi(plain)]-z0)-J_t*phi(dot))-(G_P-Jstar*J)*B_N*phi(dot)+G_N*(-B_N*phi(dot)-K_N*phi(plain))];
    
    % solve ODE
    if indx2(1)-1 ~= 0
        t0 = t(indx2(1)-1)+h1;
        t1 = t0+h2-h1;
        tspan = t0:h1:t1;
        phiInit = phi(:,indx2(1)-1);
    end
    [tNew,phiNew] = ode45(f,tspan,phiInit);
    
    % save result
    clusterID(indx2) = indx1*ones(length(tNew),1);
    t(indx2) = tNew;
    phi(:,indx2) = phiNew';
    z(:,indx2) = F*[ones(1,size(phiNew,1));phiNew(:,1:Nphi)'];
end

end