function main(pathIn,pathOut,graphicsPath,...
    spatRes,crit,frameRate,ncl,q,nf,verbose)
% MAIN - performs the forward kinematic map analysis
% 
% input:
%  PATHIN - path to track files
%  PATHOUT - path to save output
%  GRAPHICSPATH - path to save graphics
%  FOV - 200 mm^2 field of view
%  NPIXEL - nPixel^2 is total pixels in MRI frame
%  SPATRES - spatial resolution (mm)
%  CRIT - error tolerance for linear approx.
%  TR - 6 ms TR
%  FRAMERATE - frame rate assuming two TR per frame
%  NCL - number of clusters (for automatic finder), or empty array (for
%  manual finder)
%  Q - quantile below which a constriction is considered in the automatic
%  finder, empty array if using manual finder.
%  NF - number of factors in the factor model (struct array)
%  VERBOSE - controls output to MATLAB terminal
% 
% Last Updated: Nov. 17, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

% make_contour_data(pathIn,pathOut,spatRes) % data assembly

% get_Ugfa(pathOut,nf,verbose) % guided factor analysis

% if ~isempty(ncl)
%     discover_cl(pathIn,pathOut,q,ncl,spatRes) % constriction location finder
% else
%     % uses 'tvlocs.mat' in subdirectory 'data'
% end

% getTV(pathOut) % constriction degree measurement

% plotTV(pathOut,graphicsPath,spatRes) % plot constriction degrees and locations at 
                                     % one example frame

% wrapFwdMap(pathOut,crit,frameRate,nf,verbose) % forward map
% 
% rm_nonlinear_clusters(pathOut) % remove nonlinear clusters, keep only linear ones
% 
% calc_rms_error(pathOut,nf) % RMS error
% 
% plotTaskDemo(pathOut,graphicsPath,nf) % task dynamics demo

end