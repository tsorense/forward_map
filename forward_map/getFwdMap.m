function [fwd,J,resid] = getFwdMap(z,w)

int = ones(size(w,1),1);

w = [int, w];
% gamma_ls = (w'*w)\w'*z;
% nw = size(w,2);
% nz = size(z,2);
% n = size(w,1);
% SSR = sum((w*gamma_ls - z).^2,1);
% sigma2_ls = SSR/(n-nw-1);
% fwd = NaN(nw,nz);
% for i=1:nz
%     k = nw*sigma2_ls(i)/sum(gamma_ls(:,i).^2);
%     fwd(:,i) = (w'*w + k*eye(nw))\w'*z(:,i);
% end
fwd = lscov(w,z);
fwd = fwd';
J = fwd(:,2:end);
resid = sqrt(mean(sum((w*fwd' - z).^2,2)));

end