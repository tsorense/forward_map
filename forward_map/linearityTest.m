function [linear,dzdw,resid] = linearityTest(z,w,crit)

[~,dzdw,resid] = getFwdMap(z,w);
linear = resid < crit;

end