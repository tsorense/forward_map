function jacDot = getJacDot(dzdt,dwdt)

int = ones(size(dwdt,1),1);

gamma_ls = (dwdt'*dwdt)\dwdt'*dzdt;
nw = size(dwdt,2);
nz = size(dzdt,2);
n = size(dwdt,1);
SSR = sum(dwdt*gamma_ls - dzdt,1).^2;
sigma2_ls = SSR/(n-nw-1);
jacDot = NaN(nw,nz);
for i=1:nz
    k = nw*sigma2_ls(i)/sum(gamma_ls(:,i).^2);
    jacDot(:,i) = (dwdt'*dwdt + k*eye(nw))\dwdt'*dzdt(:,i);
end
jacDot = jacDot';

end