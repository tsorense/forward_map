function [lib,centers,fwd,jac,jacDot,clusterInd,linInd] = breakCluster(k,minSize,lib,curCluster,dzdt,dwdt,z,w,dzdw,resid,centers,fwd,jac,jacDot,clusterInd,linInd,linear,verbose)

% break the current cluster into k clusters
idx = kmeans(w(curCluster,:),k);

if sum(idx==1) > minSize && sum(idx==2) > minSize
    % put broken clusters in library
    tmp = zeros(length(curCluster),1);
    tmp(curCluster) = idx;
    for i=1:k
        lib=cat(1,lib,logical(tmp==i));
    end
else
    % do not break, add cluster
    [centers,fwd,jac,jacDot,clusterInd,linInd] = addCluster(curCluster,dzdt,dwdt,z,w,dzdw,resid,centers,fwd,jac,jacDot,clusterInd,linInd,linear,verbose);
end

end